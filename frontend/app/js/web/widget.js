(function () {
    var Avisits = function () {
        return {
            _url: "http://avisits-test.world-travel.uz/frontend/app/",

            _getError: function (msg) {
                return "<div class='avisits_error'>" + msg + "</div>"
            },

            _loadMarkup: function (options, step) {

                return new Promise(function (resolve, reject) {

                    var xhr = new XMLHttpRequest();
                    xhr.open('GET', window.Avisits._url + 'tpl/widget/step_' + step + '.html', true);

                    xhr.onreadystatechange = function () {
                        if (xhr.readyState != 4) return;

                        if (xhr.status !== 200) {
                            reject(xhr.status)
                        } else {
                            var container = document.getElementById("avisits" + options.id);
                            container.innerHTML = xhr.responseText;
                            resolve(container);

                            //deferred.callback(xhr.responseText);
                        }
                    };

                    xhr.send();
                });

            },

            Widget: function (options) {
                var $container = document.getElementById("avisits" + options.id);
                window.Avisits.actions = ['city', 'contacts', 'salons', 'services_group', 'services', 'employee', 'date', 'info'];
                if (!$container) return;

                // Подгружаем JS указанные в верстке
                loadJsFiles()
                    .then(function () {

                        window.Avisits._loadMarkup(options, 'city')
                            .then(function (container) {

                                $("#start").click(function () {
                                    $("#avisits" + options.id).fadeIn(200);
                                    $(".avisits-close").click(function () {
                                        $($container).fadeOut(200);
                                    });
                                });

                                loadActions($container);
                                $(".preloader").hide();
                                $(".display-none").show();

                            }, function (error) {
                                throw error
                            });

                    })

            }
        }
    };

    window.Avisits = Avisits();
    window.Avisits.step = 0;

    if (window["avisits_callbacks"]) {
        window["avisits_callbacks"].forEach(function (callback) {
            callback.call(window);
        });
    }

    function loadJsFiles() {

        return new Promise(function (resolve, reject) {

            // Подключаю стили
            var style = document.createElement('link');
            style.rel = 'stylesheet';
            style.href = "http://avisits-test.world-travel.uz//frontend/app/widget/css/styles.css";
            document.body.appendChild(style);

            // Подгружаем JQUERY
            var script = document.createElement('script');
            script.src = "http://avisits-test.world-travel.uz/frontend/app/widget/js/jquery-2.0.3.min.js";
            document.body.appendChild(script);
            script.onload = function () {

                // потом все остальные скрипты
                var listFiles = [
                    "http://avisits-test.world-travel.uz/frontend/app/widget/js/slick.min.js",
                    "http://avisits-test.world-travel.uz/frontend/app/widget/js/jquery.formstyler.js",
                    "http://avisits-test.world-travel.uz/frontend/app/widget/js/enscroll-0.6.2.min.js",
                    "http://avisits-test.world-travel.uz/frontend/app/widget/js/jquery.placeholder.min.js",
                    "http://avisits-test.world-travel.uz/frontend/app/widget/js/salon.js"
                ];

                for (var i = 0; i < listFiles.length; i++) {

                    var script = document.createElement('script');
                    script.src = listFiles[i];
                    document.body.appendChild(script);
                }

                // Подключаю карту
                var script = document.createElement('script');
                script.src = "https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false";
                document.body.appendChild(script);
                script.onload = function () {

                    var script = document.createElement('script');
                    script.src = "http://avisits-test.world-travel.uz/frontend/app/widget/js/map.js";
                    document.body.appendChild(script);

                    var script = document.createElement('script');
                    script.src = "http://avisits-test.world-travel.uz/frontend/app/widget/js/scripts.js";
                    document.body.appendChild(script);
                }


                resolve();
            }

        });
    }

    function loadActions($container) {

        var prevLoadFile = 'step_' + window.Avisits.actions[window.Avisits.step - 1] + '.html';

        $(".avisits-arrow_back").click(function () {
            window.Avisits.step--;
            var prevAction = window.Avisits.actions[window.Avisits.step];
            if (window.Avisits.step >= 0) {

                switch (prevAction) {
                    case 'contacts' :
                        getContactForm($container);
                        break;

                    case 'services_group' :
                        getServiceGroups($container);
                        break;

                    case 'salons' :
                        getListSalons($container);
                        break;

                    case 'date' :
                        getDateForm($container);
                        break;

                    case 'services' :
                        getServiceForm($container);
                        break;

                    case 'employee' :
                        getListSalonEmployees($container);
                        break;

                }
            }
        });

        $(".avisits-close").click(function () {
            window.Avisits.step = -1;
            $($container).fadeOut(200);
        });

        $("a.avisits-menu-item, .avisits-salon_buttons a, .avisits-next-item").click(function (e) {

            window.Avisits.step++;
            var $this = $(this),
                prevAction = window.Avisits.actions[window.Avisits.step - 1],
                activeAction = window.Avisits.actions[window.Avisits.step];

            if (prevAction == 'contacts') {
                var error = '';
                var contact = {
                    phone: $("input[type=tel]").val(),
                    name: $("input[name=fio]").val(),
                    email: $("input[type=email]").val()
                }

                if (!contact.phone || !contact.name || !contact.email) {
                    error = "Все поля являются обязательными для заполнения";
                }

                if (!error && !validateEmail(contact.email)) {
                    error = "Вы указали не корректный Email";
                }

                if (error) {
                    $(".error").text(error).fadeIn(50);
                    window.Avisits.step--;
                    $(".avisits-your_contacts input").keydown(function () {
                        $(".error").fadeOut(50);
                    });
                    return;
                }
                else {
                    window.Avisits.contact = contact;
                }
            }

            if ($this.attr("data-avists-city"))window.Avisits.city = $(this).attr("data-avists-city");
            if ($this.attr("data-avists-salon"))window.Avisits.salon = $(this).attr("data-avists-salon");
            if ($this.attr("data-avists-group")) {
                window.Avisits.group = {
                    id: $(this).attr("data-avists-group"),
                    name: $(this).attr("data-avists-group-name")
                }
            }
            if ($this.attr("data-avists-employe")) {
                window.Avisits.employe = {
                    'id': $(this).attr("data-avists-employe"),
                    'name': $(this).attr("data-avists-employe-name")
                };
            }
            if ($this.attr("data-avists-time")) {
                window.Avisits.time = {
                    'start': $(this).attr("data-avists-time"),
                    'finish': $(this).attr("data-avists-time-finish")
                };
            }

            if (activeAction == 'salons')getListSalons($container);
            if (activeAction == 'services_group')getServiceGroups($container);
            if (activeAction == 'services') getServiceForm($container);
            if (activeAction == 'services2') { // Корзина услуг

                if (!window.Avisits.services || window.Avisits.services.length == 0) {
                    window.Avisits.step--;
                    $(".error").text('Вы не выбрали не одну услугу').fadeIn(50);
                    $("input[type=checkbox]").change(function () {
                        $(".error").fadeOut(50);
                    });

                    return false;
                }

                $($container).load(window.Avisits._url + 'tpl/widget/' + nexLoadFile, function () {

                    var allCount = 0,
                        allPrice = 0,
                        allDuration = 0,
                        listAppend = '';

                    for (var n = 0; n < window.Avisits.services.length; n++) {
                        allCount++;
                        allPrice += +window.Avisits.services[n]['price'];
                        allDuration += +window.Avisits.services[n]['duration'];

                        listAppend += '<li><a>' + window.Avisits.services[n]['name'] + '</a></li>';
                    }

                    $(".avisits-services #scrollbox ul").append(listAppend || '');
                    var listRow = $(".avisits-row li");
                    listRow.eq(0).find('strong').text(allCount);
                    listRow.eq(1).find('strong').text(allPrice);
                    listRow.eq(2).find('strong').text(allDuration);

                    loadActions($container);
                })
            }
            if (activeAction == 'employee')getListSalonEmployees($container);
            if (activeAction == 'date')getDateForm($container);
            if (activeAction == 'contacts')getContactForm($container);
            if (activeAction == 'info') getInfoForm($container);

            if (e) {
                e.preventDefault();
                e.stopPropagation();
            } else {
                return false;
            }

        })

    }

    function getInfoForm($container) {
        $($container).load(window.Avisits._url + 'tpl/widget/step_info.html', function () {

            var listServices = window.Avisits.services,
                serviceAppend = '';

            for (var i = 0; i < listServices.length; i++) {
                serviceAppend += '<li><p>' + listServices[i]['name'] + '</p><span>' + getTime(listServices[i]['duration'], true) + '</span><strong>' + getPrice(listServices[i]['price']) + 'р</strong></li>';
            }

            $(".avisits-ser ul").append(serviceAppend);

            if(window.Avisits.employe.id >0)
                $(".avisits-specialist h4").text(window.Avisits.employe.name);
            else $(".avisits-specialist").hide();

            $(".avisits-contact-info p").text(window.Avisits.contact.name);
            $(".avisits-contact-info span").html(window.Avisits.contact.phone + '<a href="mailto:' + window.Avisits.contact.email + '">' + window.Avisits.contact.email + '</a>');

            loadActions($container);

            $(".preloader").hide();
            $(".display-none").show();

        });
    }

    function getDateForm($container) {
        $.ajax({
            url: "list_schedules.json",
            type: "GET",
            success: function (schedule) {

                $($container).load(window.Avisits._url + 'tpl/widget/step_date.html', function () {

                    var listAppend = '',
                        currentItem = 0,
                        activeTime = false;

                    getListsDays();

                    $(".avisits-date_slider").slick({
                        infinite: true,
                        slidesToShow: 7,
                        slidesToScroll: 1,
                        //rtl: true,
                        dots: false
                    });

                    $('.avisits-date_slider').on('afterChange', function (event, slick, currentSlide, nextSlide) {
                        alert('h');
                    });

                    for (var n = 0; n < schedule['morning'].length; n++, currentItem++) {
                        if (currentItem == 0) {
                            listAppend += '<tr>';
                        }

                        activeTime = (
                            window.Avisits.time &&
                            schedule['morning'][n]['start'] == window.Avisits.time.start &&
                            schedule['morning'][n]['finish'] == window.Avisits.time.finish
                        );

                        //
                        listAppend += '<td><a href="#" class="avisits-next-item' + (activeTime ? ' avisits-active' : '') + '" data-avists-time="' + schedule['morning'][n]['start'] + '" data-avists-time-finish="' + schedule['morning'][n]['finish'] + '" >' + schedule['morning'][n]['start'] + '<span>' + schedule['morning'][n]['finish'] + '</span></a></td>';
                        if (currentItem == 3) {
                            listAppend += '<tr>';
                            currentItem = 0;
                        }
                    }
                    $(".avisits-item-morning table").append(listAppend);

                    currentItem = 0;
                    listAppend = '';
                    for (var n = 0; n < schedule['day'].length; n++, currentItem++) {

                        activeTime = (
                            window.Avisits.time &&
                            schedule['day'][n]['start'] == window.Avisits.time.start &&
                            schedule['day'][n]['finish'] == window.Avisits.time.finish
                        );

                        if (currentItem == 0) {
                            listAppend += '<tr>';
                        }
                        listAppend += '<td><a href="#" class="avisits-next-item' + (activeTime ? ' avisits-active' : '') + '" data-avists-time="' + schedule['day'][n]['start'] + '" data-avists-time-finish="' + schedule['day'][n]['finish'] + '" >' + schedule['day'][n]['start'] + '<span>' + schedule['day'][n]['finish'] + '</span></a></td>';
                        if (currentItem == 3) {
                            listAppend += '<tr>';
                            currentItem = 0;
                        }
                    }
                    $(".avisits-item-day table").append(listAppend);

                    currentItem = 0;
                    listAppend = '';
                    for (var n = 0; n < schedule['evening'].length; n++, currentItem++) {

                        activeTime = (
                            window.Avisits.time &&
                            schedule['evening'][n]['start'] == window.Avisits.time.start &&
                            schedule['evening'][n]['finish'] == window.Avisits.time.finish
                        );

                        if (currentItem == 0) {
                            listAppend += '<tr>';
                        }
                        listAppend += '<td><a href="#" class="avisits-next-item' + (activeTime ? ' avisits-active' : '') + '" data-avists-time="' + schedule['evening'][n]['start'] + '" data-avists-time-finish="' + schedule['evening'][n]['finish'] + '" >' + schedule['evening'][n]['start'] + '<span>' + schedule['evening'][n]['finish'] + '</span></a></td>';
                        if (currentItem == 3) {
                            listAppend += '<tr>';
                            currentItem = 0;
                        }
                    }

                    $(".avisits-item-evening table").append(listAppend);

                    loadActions($container);

                    $(".preloader").hide();
                    $(".display-none").show();

                });
            }
        })
    }

    function getListsDays() {
        var now = new Date(),
            month = now.getMonth(),
            monthName = '',
            dateNum = now.getDay(),
            currentDate = window.Avisits.date && window.Avisits.date.day ? window.Avisits.date.day : now.getDate(),
            countDays = (new Date(now.getFullYear(), now.getMonth() + 1, 0)).getDate(),
            listDaysAppend = '';

        var days = ['вс', 'пн', 'вт', 'ср', 'чт', 'пт', 'сб'];

        switch (month) {
            case 0 :
                monthName = "Январь";
            case 1:
                monthName = "Февраль";
                break;
            case 2:
                monthName = "Март";
                break;
            case 3:
                monthName = "Апрель";
                break;
            case 4:
                monthName = "Май";
                break;
            case 5:
                monthName = "Июнь";
                break;
            case 6:
                monthName = "Июль";
                break;
            case 7:
                monthName = "Август";
                break;
            case 8:
                monthName = "Сентябрь";
                break;
            case 9:
                monthName = "Октябрь";
                break;
            case 10 :
                monthName = "Ноябрь";
                break;
            case 11 :
                monthName = "Декабрь";
                break;
        }

        $(".avisits-date h2").text(monthName);

        for (var i = now.getDate(); i <= countDays; i++) {
            listDaysAppend +=
                '<div class="avisits-item">' +
                '<div class="avisits-col' + ( i == currentDate ? ' avisits-active' : '') + '">' +
                '<a href="#" data-avists-date-month="' + month + '" data-avists-date-day="' + i + '">' +
                '<strong>' + i + '</strong>' +
                '<p>' + days[dateNum] + '</p>' +
                '</a>' +
                '</div>' +
                '</div>';

            if (dateNum < 6)dateNum++;
            else dateNum = 0;
        }

        $(".avisits-date_slider").append(listDaysAppend);

        $(".avisits-date_slider .avisits-item a").click(function (e) {

            $(".avisits-date_slider .avisits-item .avisits-active").removeClass("avisits-active");
            var parentDiv = $(this.parentNode);
            var $this = $(this);
            parentDiv.addClass("avisits-active");
            window.Avisits.date = {
                month: $this.attr("data-avists-date-month"),
                day: $this.attr("data-avists-date-day")
            };

            e.preventDefault();
            e.stopPropagation();
        })

        $(".preloader").hide();
        $(".display-none").show();
    }

    function getServiceGroups($container) {

        // http://avisits.com:8080/api/offergroups
        $.ajax({
            url: "list_salons_group_services.json",
            type: "GET",
            success: function (listGroupServices) {

                $($container).load(window.Avisits._url + 'tpl/widget/step_services_group.html', function () {

                    var items = '';
                    for (var i = 0; i < listGroupServices.length; i++) {
                        items += '<li><a class="avisits-next-item" data-avists-group="' + listGroupServices[i]['id'] + '" data-avists-group-name="' + listGroupServices[i]['name'] + '" href="#">' + listGroupServices[i]['name'] + '</li>';
                    }

                    $("#scrollbox ul").append(items);

                    loadActions($container);

                    $(".preloader").hide();
                    $(".display-none").show();

                });
            }
        });
    }

    function getListSalons($container) {

        $.ajax({
            url: "list_salons.json",
            type: "GET",
            success: function (data) {
                window.Avisits.listSalons = data;
                $($container).load(window.Avisits._url + 'tpl/widget/step_salons.html', function () {

                    for (var i = 0; i < window.Avisits.listSalons.length; i++) {
                        $(".avisits-salon_box ul").append(
                            '<li>' +
                            '<a class="mega__trigger avisits-next-item" data-mega="#point1" data-avists-salon="' + window.Avisits.listSalons[i]['id'] + '" href="#">' +
                            '<div class="avisits-img"><img src="http://avisits/frontend/app/widget/images/salon_box/img1.jpg" alt=""></div>' +
                            '<div class="avisits-text">' +
                            '<h4>' + window.Avisits.listSalons[i]['name'] + '</h4>' +
                            '<p>пусто</p>' +
                            '</div>' +
                            '</a>' +
                            '</li>'
                        );
                    }

                    loadActions($container);

                    $(".preloader").hide();
                    $(".display-none").show();
                });

            }
        });
    }

    function getServiceForm($container) {
        if (!window.Avisits.group || !window.Avisits.group) {
            window.Avisits.step--;
            return false;
        }

        // http://avisits.com:8080/api/offergroups
        $.ajax({
            url: "list_group_services.json",
            type: "GET",
            success: function (listServices) {

                $($container).load(window.Avisits._url + 'tpl/widget/step_services.html', function () {

                    $(".avisits-block_title").text(window.Avisits.group.name);
                    var items = '';
                    for (var i = 0; i < listServices.length; i++) {

                        var subItems = '';
                        for (var n = 0; n < listServices[i]['offers'].length; n++) {

                            subItems += '<li>' +
                                '<div class="avisits-box">' +
                                '<input type="checkbox" data-avists-service-name="' + listServices[i]['offers'][n]['name'] + '" data-avists-service-price="' + listServices[i]['offers'][n]['price'] + '" data-avists-service-id="' + listServices[i]['offers'][n]['id'] + '" data-avists-service-duration="' + listServices[i]['offers'][n]['duration'] + '" id="avisits-d' + listServices[i]['offers'][n]['id'] + '" name="avisits-services[]" value="' + listServices[i]['offers'][n]['id'] + '" />' +
                                '<label for="avisits-d' + listServices[i]['offers'][n]['id'] + '"><span></span>' + listServices[i]['offers'][n]['name'] + '</label>' +
                                '</div>' +
                                '<span>' + getTime(listServices[i]['offers'][n]['duration'], true) + '</span>' +
                                '<p>' + getPrice(listServices[i]['offers'][n]['price']) + 'р</p>' +
                                '</li>';
                        }
                        if (subItems) {
                            items +=
                                '<li>' +
                                '<a href="#">' + listServices[i]['name'] + '</a>' +
                                '<ul>' + subItems + '</ul>' +
                                '</li>';
                        }

                    }

                    var activeGroup = '';
                    $(".avisits-select-services ul").append(items);
                    $(".avisits-select-services ul a").click(function () {
                        var ul = $(this.parentNode).find('ul');
                        if (activeGroup && activeGroup != ul)activeGroup.fadeOut(50);
                        activeGroup = ul;
                        ul.fadeToggle(50);
                    });

                    if (window.Avisits.services && window.Avisits.services.length > 0) {
                        for (var m = 0; m < window.Avisits.services.length; m++) {
                            $('#avisits-d' + window.Avisits.services[m]['id'])[0].checked = true;
                        }
                    }

                    if (window.Avisits.services && window.Avisits.services.length > 0) {
                        serviceListChange();
                    }

                    $(".avisits-select-services ul input[type=checkbox]").change(function () {
                        serviceListChange();
                    })

                    loadActions($container);

                    $(".preloader").hide();
                    $(".display-none").show();
                });
            }
        });
    }

    function serviceListChange() {
        var listCheckbox = $('.avisits-select-services ul input'),
            allCount = 0,
            allPrice = 0,
            allDuration = 0,
            selectedItems = '';

        window.Avisits.services = [];
        for (var n = 0; n < listCheckbox.length; n++) {
            if (listCheckbox[n].checked) {
                var $this = $(listCheckbox[n]);
                allCount++;
                allPrice += +$this.attr('data-avists-service-price');
                allDuration += +$this.attr('data-avists-service-duration');

                window.Avisits.services.push({
                    'id': $this.attr('data-avists-service-id'),
                    'name': $this.attr('data-avists-service-name'),
                    'price': $this.attr('data-avists-service-price'),
                    'duration': $this.attr('data-avists-service-duration')
                })

                selectedItems += '<li>' +
                    '<div class="avisits-box">' +
                    '<input type="checkbox" id="d' + $this.attr('data-avists-service-id') + '" name="cc" checked/>' +
                    '<label for="d' + $this.attr('data-avists-service-id') + '"><span></span>' + $this.attr('data-avists-service-name') + '</label>' +
                    '</div>' +
                    '<span>' + getTime( $this.attr('data-avists-service-duration'), true ) + '</span>' +
                    '<p>' + getPrice($this.attr('data-avists-service-price')) + 'р</p>' +
                    '</li>';
            }
        }

        var listRow = $(".avisits-row li");
        listRow.eq(0).find('strong').text(allCount);
        listRow.eq(1).find('strong').text(getPrice(allPrice));
        listRow.eq(2).find('strong').text(getTime(allDuration, false));

        $(".avisits-form_top ul").html(selectedItems);
        $(".avisits-form_top ul input[type=checkbox]").change(function () {
            $('#avisits-' + this.id)[0].checked = this.checked;
            serviceListChange();
        })
    }

    function getContactForm($container) {
        $($container).load(window.Avisits._url + 'tpl/widget/step_contacts.html', function () {

            if (window.Avisits.contact) {
                if (window.Avisits.contact.phone)$("input[name=phone]").val(window.Avisits.contact.phone);
                if (window.Avisits.contact.name)$("input[name=fio]").val(window.Avisits.contact.name);
                if (window.Avisits.contact.email)$("input[name=email]").val(window.Avisits.contact.email);
            }

            $(".avisits-your_contacts input[name=email]").on('input keyup', function () {
                if (validateEmail(this.value)) {
                    $(this.parentNode).addClass('avisits-ok');
                    $(this.parentNode).removeClass('avisits-error');
                }
                else {
                    $(this.parentNode).removeClass('avisits-ok');
                    $(this.parentNode).addClass('avisits-error');
                }
            });

            loadActions($container);
            $(".preloader").hide();
            $(".display-none").show();

        });
    }

    function getListSalonEmployees($container) {
        $.ajax({
            url: "list_salonemployees.json",
            type: "GET",
            success: function (listEmployees) {

                $($container).load(window.Avisits._url + 'tpl/widget/step_employee.html', function () {

                    var listAppend = '';

                    for (var n = 0; n < listEmployees.length; n++) {

                        listAppend += '<li >' +
                            '<div class="avisits-img"><img src="images/professional_choice/img1.jpg" alt=""></div>' +
                            '<div class="avisits-text">' +
                            '<h4><a href="#" class="avisits-next-item" data-avists-employe="' + listEmployees[n]['id'] + '" data-avists-employe-name="' + listEmployees[n]['accessProfile']['name'] + '">' + listEmployees[n]['accessProfile']['name'] + '</a></h4>' +
                            '<p>Парикмахер-стилист?</p>' +
                            '</div>' +
                            '</li>';
                    }

                    $(".avisits-professional_box #scrollbox ul").append(listAppend);

                    loadActions($container);
                    $(".preloader").hide();
                    $(".display-none").show();
                });
            }
        })
    }

    function validateEmail($email) {
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        return emailReg.test($email);
    }

    function getTime(data, needLiteral) {
        var hours = Math.floor(data / 60),
            minut = data - hours*60;

        return (hours ? hours+'ч ' : '' )+(minut ? minut+'м' : '0м')
    }

    function getPrice(data) {
        var price = Number.prototype.toFixed.call(parseFloat(data) || 0, 0),
            price_sep = price.replace(/(\D)/g, ","),
            price_sep = price_sep.replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1 ");

        return price_sep;
    }

})();